<?php
/*error_reporting(E_ALL & ~E_NOTICE);
*/session_start();
require_once"connection.php";
/*include("update.php");*/

$all_contacts = "SELECT * FROM contacts";
$sql_all_contacts = $connect->query($all_contacts);
$total = $sql_all_contacts->num_rows;

if (isset($_SESSION['id'])) {
	$userId = $_SESSION['id'];
	$username = $_SESSION['username'];
} else {
	header('Location: login.php');
	die();
}

//Insert New Contact
if (isset($_POST['submit_new'])) {
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$telephone = $_POST['telephone'];
	$email = $_POST['email'];
	$birthday = $_POST['birthday'];
	$insert_contact = "INSERT INTO contacts (first_name, last_name, telephone, email, birthday) values ('$first_name', '$last_name', '$telephone', '$email', '$birthday')";
	$sql_insert_contact = $connect->query($insert_contact);
	if($sql_insert_contact) {
        header('Location: index.php');
        exit;
    }
}
//Update Existing
if (isset($_GET['id'])) {
	$id = $_GET['id'];
	$get_contact = "select * from contacts where id = '$id'";
	$sql_get_contact = $connect->query($get_contact);
	$row = mysqli_fetch_array($sql_get_contact);
}

if (isset($_POST['submit_update'])) {
	$id = $_POST['id'];
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$telephone = $_POST['telephone'];
	$email = $_POST['email'];
	$birthday = $_POST['birthday'];
	/*$sql_date = $birthday->format('Y-m-d');*/
	$update_contact = "UPDATE contacts SET first_name='$first_name', last_name='$last_name', telephone='$telephone', email='$email', birthday='$birthday' where id='$id'";
	echo $update_contact;
	$sql_update_contact = $connect->query($update_contact);
	if($sql_update_contact) {
		header('Location: index.php');
		exit;
	}
}

?>


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Phonebook Redo</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="style.css" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

<div id="header">
	<p>Phonebook Redo</p>
</div>

<div class="container">
	<div class="buttons">
		<div class="floatLeft">
			Total Entries:
			<span class="badge"><?php echo $total; ?></span>
		</div>
		<button class="floatRight" type="button" data-toggle="modal" data-target="#new_contact">New Contact</button>
	</div>
<form method="post">
	<div class="modal fade" id="new_contact" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
       				<h4 class="modal-title">Modal Header</h4>
       			</div>

       			<div class="modal-body">
       				<div class="form-group">
						<label class="control-label col-sm-2" for="first_name">First Name:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="first_name" placeholder="Insert Name" name="first_name">
						</div>
					</div>

      				<div class="form-group">
						<label class="control-label col-sm-2" for="last_name">Last Name:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="last_name" placeholder="Insert Name" name="last_name">
						</div>
					</div>

      				<div class="form-group">
						<label class="control-label col-sm-2" for="telephone">Telephone:</label>
						<div class="col-sm-10">
							<input type="text" class="telephone" id="telephone" placeholder="Insert Phone Number" name="telephone">
						</div>
					</div>

      				<div class="form-group">
						<label class="control-label col-sm-2" for="email">Email:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="email" placeholder="Insert Email" name="email">
						</div>
					</div>
     				<div class="form-group">
     					<label class="control-label col-sm-2" for="birthday">Birthday:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="birthday" placeholder="Update birthday" name="birthday">
						</div>
					</div>


       			</div>

       			<div class="modal-footer">
       				<button type="submit" class="btn btn-success float-sm-left" name="submit_new">Submit</button>
       				<button type="button" class="btn btn-default float-sm-right" data-dismiss="modal">Close</button>
       			</div>
			</div>
		</div>
	</div>
</form>

<div class="TableThing">
	<table class="table table-bordered table-hover">
		<thead class="table_head">
			<tr>
				<th>Name:</th>
				<th>Cell Phone:</th>
				<th>Email: </th>
				<th>Birthday:</th>
				<th class="lastcolumn">Action:</th>
			</tr>
		</thead>

		<tbody>
			<?php while ($row = mysqli_fetch_assoc($sql_all_contacts)) { ?>
				<tr>
					<td>
						<img src="cps/default.jpg" style="width: 50px;">
						<?php
							echo $row['first_name'] . " " . $row['last_name'];
						?>
						</td>
					<td>
						<?php
							echo $row['telephone'];
						?>
					</td>
					<td>
						<?php
							echo $row['email'];
						?>
					</td>
					<td>
						<?php
							function datentime() {
									global $row;
									$timestamp = strtotime($row['birthday']);
									$day = date("d",$timestamp);
									$month = date("m",$timestamp);
									$year = date("Y",$timestamp);
									echo $day."-".$month."-".$year;}
									echo datentime();
						?>
					</td>
					<td class="lastcolumn"><button class="edit_button btn btn-warning" data-toggle="collapse" data-target=".edit<?php echo $row['id']?>">Quick Edit</button></td>
					<td><a href="update.php?id=<?php echo $row['id']; ?>">Update</a></td>
				</tr>

				<form method="post">
					<tr class="collapse edit<?php echo $row['id']?>">
						<td>
							<div class="form-group">
								<!-- <label class="control-label col-sm-2" for="first_name">First Name:</label> -->
								<div class="col-sm-10">
									<input type="text" class="form-control" id="first_name" placeholder="Update Name" name="first_name" value="<?php echo $row['first_name'] ?>">
								</div>
							</div>
							<div class="form-group">
								<!-- <label class="control-label col-sm-2" for="pwd">Password:</label> -->
								<div class="col-sm-10">
									<input type="text" class="form-control" id="last_name" placeholder="Update Last Name" name="last_name" value="<?php echo $row['last_name'] ?>">
								</div>
							</div>
						</td>
						<td>
							<div class="form-group">
								<div class="col-sm-10">
									<input type="text" class="form-control" id="telephone" placeholder="Update Phone Number" name="telephone" value="<?php echo $row['telephone'] ?>">
								</div>
							</div>
						</td>
						<td>
							<div class="form-group">
								<div class="col-sm-10">
									<input type="text" class="form-control" id="email" placeholder="Update Email" name="email" value="<?php echo $row['email'] ?>">
								</div>
							</div>
						</td>
						<td>
							<div class="form-group">
								<div class="col-sm-10">
									<input type="text" class="form-control" id="birthday" placeholder="Update birthday" name="birthday" value="<?php echo $row['birthday']; ?>">
								</div>
							</div>
						</td>
						<td class="lastcolumn">
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<input type="hidden" name="id" value="<?php echo $row['id'] ?>">
									<button type="submit" class="btn btn-success float-sm-right" name="submit_update">Update!</button>

									<a class="btn btn-danger float-sm-left" href="delete.php?id=<?php echo $row['id'] ?>">Delete!</a>
								</div>
							</div>
						</td>
					</tr>
				</form>
			<?php } ?>
		</tbody>
	</table>

	</div>
</div>
<form action="logout.php">
	<input type="submit" value="Log Out." name="logout" />
</body>
</html>