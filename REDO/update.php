
<?php
require_once"connection.php";
ob_start();

if (isset($_GET['id'])) {
	$id = $_GET['id'];
	$get_contact = "select * from contacts where id = '$id'";
	$sql_get_contact = $connect->query($get_contact);
	$row = mysqli_fetch_array($sql_get_contact);
}

if (isset($_POST['submit_update'])) {
	$id = $_POST['id'];
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$telephone = $_POST['telephone'];
	$email = $_POST['email'];
	$birthday = $_POST['birthday'];

	$update_contact = "UPDATE contacts SET first_name='$first_name', last_name='$last_name', telephone='$telephone', email='$email', birthday='$birthday' where id='$id'";
	echo $update_contact;
	$sql_update_contact = $connect->query($update_contact);

	if ($sql_update_contact) {
			header( 'Location: index.php' );
			exit();
		}
}
?>


<!doctype html>
<html>
<head>
<meta charset="utf-8">

<title>Phonebook Update</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="style.css" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

<form method="post">

<table class="table">
	<input type="hidden" name="id" value="<?php echo $row['id'] ?>">
	<tr>
		<td>
			<div class="form-group">
				<div class="col-sm-10">
					<input type="text" class="form-control" id="first_name" placeholder="Update Name" name="first_name" value="<?php echo $row['first_name'] ?>">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-10">
					<input type="text" class="form-control" id="last_name" placeholder="Update Last Name" name="last_name" value="<?php echo $row['last_name'] ?>">
				</div>
			</div>
		</td>
		<td>
			<div class="form-group">
				<div class="col-sm-10">
					<input type="text" class="form-control" id="telephone" placeholder="Update Phone Number" name="telephone" value="<?php echo $row['telephone'] ?>">
				</div>
			</div>
		</td>
		<td>
			<div class="form-group">
				<div class="col-sm-10">
					<input type="text" class="form-control" id="email" placeholder="Update Email" name="email" value="<?php echo $row['email'] ?>">
				</div>
			</div>
		</td>
		<td>
			<div class="form-group">
				<div class="col-sm-10">
					<input type="text" class="form-control" id="birthday" placeholder="Update birthday" name="birthday" value="<?php echo $row['birthday'] ?>">
				</div>
			</div>
		</td>
		<td class="lastcolumn">
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-success float-sm-right" name="submit_update">Update!</button>

				</div>
			</div>
		</td>
	</tr>
</table>
</form>



</body>
</html>
